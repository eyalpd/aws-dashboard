import { Injectable } from '@angular/core';

import { accessKey, secretKey } from '../../.env';

@Injectable({
  providedIn: 'root'
})
export class TokensService {
  private accessKey: string;
  private secretKey: string;

  constructor() { }

  getAccessKey() {
    return this.accessKey;
  }

  getSecretKey() {
    return this.secretKey;
  }
}
