import { Component, OnInit } from '@angular/core';
import * as AWS from 'aws-sdk';
import * as R from 'ramda';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subject } from 'rxjs/internal/Subject';
import { pipe } from 'rxjs/internal/util/pipe';
import { from as rxFrom } from 'rxjs/internal/observable/from';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { map } from 'rxjs/internal/operators/map';
import { filter } from 'rxjs/internal/operators/filter';
import { tap } from 'rxjs/internal/operators/tap';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { catchError } from 'rxjs/internal/operators/catchError';
import { retry } from 'rxjs/internal/operators/retry';

import { instanceTypesText } from '../../../ext-data/instance-types';
import { productDescsText } from '../../../ext-data/product-descs';
import { accessKey, secretKey } from '../../../../.env';
import { demoLineData } from '../../../ext-data/demo-line-data';

interface AvailabilityZoneInfo {
  RegionName: string;
  State: string;
  ZoneId: string;
  ZoneName: string;
}

interface InstanceType {
  name: string;
  id: string;
}

interface ProductDescription {
  name: string;
  id: string;
}

interface SpotPriceHistoryInst {
  AvailabilityZone: string;
  InstanceType: string;
  ProductDescription: string;
  SpotPrice: string;
  Timestamp: string;
}

interface SpotPriceHistoryResp {
  NextToken: string;
  SpotPriceHistory: SpotPriceHistoryInst[];
}

function useStream(obs$, unsub$) {
  return pipe(
    () => obs$,
    takeUntil(unsub$),
  )(null).subscribe();
}

function tapLog(str) {
  return tap(logData => console.log('***', str, '***', logData));
}

function switchOn(hash, val, defaultFn) {
  if (R.has(val, hash)) {
    return hash[val]();
  }

  return defaultFn();
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  unsub$ = new Subject<void>();
  public availZones$ = new BehaviorSubject<Partial<AvailabilityZoneInfo>[]>([]);
  public instanceTypes$ = new BehaviorSubject<InstanceType[]>([]);
  public productDescriptions$ = new BehaviorSubject<ProductDescription[]>([]);
  public graphData$ = new BehaviorSubject<any[]>([]);
  public selectedAvailZone$ = new BehaviorSubject<AvailabilityZoneInfo>(null);
  public selectedInstanceType$ = new BehaviorSubject<InstanceType>(null);
  public selectedProductDescription$ = new BehaviorSubject<ProductDescription>(null);
  spotPriceHistory$ = new BehaviorSubject<any[]>([]);
  maxPrice$ = new BehaviorSubject<number>(0);
  minPrice$ = new BehaviorSubject<number>(0);
  avgPrice$ = new BehaviorSubject<number>(0);
  onDemandPrice$ = new BehaviorSubject<number>(0);
  isPriceBelowDemand$ = new BehaviorSubject<boolean>(false);

  constructor() { }

  ngOnInit() {
    this.setAws();
    useStream(this.streamInstanceTypes(), this.unsub$);
    useStream(this.streamAvailZones(), this.unsub$);
    useStream(this.streamProductDescs(), this.unsub$);
    useStream(this.streamSpotPricesData(), this.unsub$);
    useStream(this.streamGraphGeneration(), this.unsub$);
    useStream(this.streamAggregateSpotPriceData(), this.unsub$);
    useStream(this.streamSpotOnDemandData(), this.unsub$);
    useStream(this.streamIsPriceBelowDemand(), this.unsub$);
  }

  setAws() {
    // const myCredentials = new AWS.CognitoIdentityCredentials({IdentityPoolId:'IDENTITY_POOL_ID'});
    const myCredentials = new AWS.Credentials(accessKey, secretKey);
    const myConfig = new AWS.Config({
      credentials: myCredentials, region: 'us-east-1',
    });
    // AWS.config.update({region:'us-east-1'});
    AWS.config.update(myConfig);
  }

  streamInstanceTypes() {
    return pipe(
      () => rxFrom([instanceTypesText]),
      map((txt) => txt.split('|')),
      map(types => types.map(typeInst => typeInst.trim())),
      map(types => types.map(typeInst => ({ id: typeInst, name: typeInst }))),
      tap(instTypes => this.instanceTypes$.next(instTypes)),
    )(null);
  }

  getAvailZonesObs(): Observable<AvailabilityZoneInfo[]> {
    return Observable.create((observer) => {
      const ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });
      const params = {};
      ec2.describeAvailabilityZones(params, (err, data) => {
        if (err) {
          console.log('Error', err);
          observer.error(err);
        } else {
          console.log('Availability Zones: ', data.AvailabilityZones);
          observer.next(data.AvailabilityZones);
          observer.complete();
        }
      });
    });
  }

  streamAvailZones() {
    return pipe(
      () => this.getAvailZonesObs(),
      tapLog('*** availability zones'),
      tap((availZones: any) => this.availZones$.next(availZones)),
    )(null);
  }

  streamProductDescs() {
    return pipe(
      () => rxFrom([productDescsText]),
      map((txt) => txt.split('|')),
      map(descs => descs.map(desc => desc.trim())),
      map(descs => descs.map(desc => ({ id: desc, name: desc }))),
      tap(descs => this.productDescriptions$.next(descs)),
    )(null);
  }

  streamSpotPricesData() {
    return pipe(
      () => combineLatest(
        this.selectedAvailZone$,
        this.selectedInstanceType$,
        this.selectedProductDescription$,
      ),
      tapLog('spot prices 1'),
      filter((params: any[]) => !params.some(param => R.isNil(param))),
      map(([availZone, instanceType, productDesc]) => {
        return {
          AvailabilityZone: availZone,
          InstanceTypes: [
            instanceType,
          ],
          ProductDescriptions: [
            productDesc,
          ],
          StartTime: new Date(new Date().setDate(new Date().getDate() - 60)),
          EndTime: new Date(),
        };
      }),
      tapLog('spot prices 2'),
      switchMap((params) => {
        const ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });
        return Observable.create((observer) => {
          ec2.describeSpotPriceHistory(params, (err, data) => {
            if (err) {
              console.log(err, err.stack);
              observer.error(err);
              return;
            }

            observer.next(data.SpotPriceHistory);
            observer.complete();
          });
        });
      }),
      tapLog('spot prices 3'),
      map((spotPriceHistory: any[]) => spotPriceHistory.map((priceInfo) => ({
        ...priceInfo,
        spotPriceNum: parseFloat(priceInfo.SpotPrice),
      }))),
      tap(spotPriceHistory => this.spotPriceHistory$.next(spotPriceHistory as any)),
    )(null);
  }

  streamAggregateSpotPriceData() {
    return pipe(
      () => this.spotPriceHistory$,
      tapLog('agg 1'),
      map((prices: any[]) => prices.reduce(({ max, min, total }, val, index) => {
        const newMax = max < val.spotPriceNum ? val.spotPriceNum : max;
        const newMin = min > val.spotPriceNum ? val.spotPriceNum : min;
        const newTotal = total + val.spotPriceNum;
        const newAvg = newTotal / (index + 1);

        return { max: newMax, min: newMin, total: newTotal, avg: newAvg };
      }, { max: Number.NEGATIVE_INFINITY, min: Number.POSITIVE_INFINITY, total: 0 })),
      tapLog('agg 2'),
      tap(({ max, min, total, avg}) => {
        this.maxPrice$.next(max);
        this.minPrice$.next(min);
        this.avgPrice$.next(avg);
      }),
    )(null);
  }

  streamSpotOnDemandData() {
    const pricing = new AWS.Pricing({ apiVersion: '2017-10-15' });

    return pipe(
      () => combineLatest(
        this.selectedInstanceType$,
        this.selectedProductDescription$,
      ),
      tapLog('on demand 1'),
      tap(() => this.onDemandPrice$.next(0)),
      filter((params: any[]) => !params.some(param => R.isNil(param))),
      tapLog('on demand 2'),
      /*
      () => {
        const params = {
          FormatVersion: 'aws_v1',
          MaxResults: 1,
          ServiceCode: 'AmazonEC2'
       };

       return Observable.create((observer) => {
         pricing.describeServices(params, function(err, data) {
           if (err) {
             console.log(err, err.stack); // an error occurred
             observer.error(err);
             return;
           }

           observer.next(data);
           observer.complete();
         });
       });
      },
      switchMap(() => {
         const params = {
           // attributeName: "instance",
           // attributeName: "instanceFamily",
           // AttributeName: "operatingSystem",
           // AttributeName: "instanceFamily",
           // AttributeName: "instanceType",
           AttributeName: "operatingSystem",
           // AttributeName: "locationType",

           MaxResults: 100,
           ServiceCode: "AmazonEC2"
         };

         return Observable.create((observer) => {
           pricing.getAttributeValues(params, function(err, data) {
             if (err) {
               console.log(err, err.stack); // an error occurred
               observer.error(err);
               return;
             }

             observer.next(data);
             observer.complete();
           });
         });
      }),
      tapLog('*** on demand 2.5'),
      */
      switchMap(([instanceType, operatingSystem]) => {
        const opSystem = switchOn({
          'Linux/UNIX': () => 'Linux',
          'SUSE Linux': () => 'SUSE',
        }, operatingSystem, () => operatingSystem);

        const params = {
          Filters: [
            {
              Field: "instanceType",
              Type: "TERM_MATCH",
              // Value: "m4.large"
              Value: instanceType,
            },
            {
              Field: 'locationType',
              Type: "TERM_MATCH",
              Value: 'AWS Region',
            },
            {
              Field: 'location',
              Type: "TERM_MATCH",
              Value: 'US East (N. Virginia)',
            },
            {
              Field: 'operatingSystem',
              Type: "TERM_MATCH",
              // Value: 'Linux',
              Value: opSystem,
            },
          ],
          FormatVersion: "aws_v1",
          MaxResults: 1,
          ServiceCode: 'AmazonEC2',
        };

        return pipe(
          () => Observable.create((observer) => {
            pricing.getProducts(params, (err, data) => {
              if (err) {
                console.log(err, params, err.stack); // an error occurred
                observer.error(err);
                return;
              }

              console.log('*** got on demand price', data.PriceList),
              observer.next(data.PriceList);
              observer.complete();
            });
          }),
          retry(1),
        )(null);
      }),
      tapLog('on demand 3'),
      filter(([priceInfo]) => !R.isNil(priceInfo)),
      tapLog('on demand 3.5'),
      map(([priceInfo]) => {
        return R.pipe(
          () => R.path(['terms', 'OnDemand'], priceInfo),
          (partial) => {
            const key = R.keys(partial)[0];
            return partial[key];
          },
          partial => R.path(['priceDimensions'], partial),
          (partial) => {
            const key = R.keys(partial)[0];
            return partial[key];
          },
          partial => R.path(['pricePerUnit', 'USD'], partial),
          price => parseFloat(price),
        )(null);
      }),
      tapLog('on demand 4'),
      tap((price: number) => this.onDemandPrice$.next(price)),
      catchError((err, obs) => {
        console.error('error getting price', err);
        return obs;
      })
    )(null);
  }

  streamIsPriceBelowDemand() {
    return pipe(
      () => combineLatest(
        this.onDemandPrice$,
        this.avgPrice$,
      ),
      tapLog('isPrice 1'),
      map(([onDemandPrice, avgPrice]) => avgPrice < onDemandPrice),
      tapLog('isPrice 2'),
      tap((isBelow: boolean) => this.isPriceBelowDemand$.next(isBelow)),
    )(null);
  }

  streamGraphGeneration() {
    return pipe(
      () => this.spotPriceHistory$,
      map(spotPriceHistory => this.generateGraphDataFromSpotPriceHistory(spotPriceHistory)),
      tapLog('graph data 1'),
      tap(graphData => this.graphData$.next(graphData as any)),
      catchError((err, obs) => {
        console.error('*** error getting prices', err);
        return obs;
      })
    )(null);
  }

  onDestory() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  generateGraphDataFromSpotPriceHistory(history): any[] {
    return [{
      name: 'spot price',
      series: history.map((data) => ({ name: data.Timestamp, value: data.spotPriceNum })),
    }];
  }
}
